from django.test import TestCase, Client
from django.urls import reverse, resolve
# from .models import 
from .views import index, about

# Create your tests here.

class ArtikelTest(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/')
        self.assertEqual(found_func.func, index)

    def test_event_using_template(self):
        template = Client().get('/')
        self.assertTemplateUsed(template, 'home/index.html')

    def test_index_GET(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "home/index.html")